import express from 'express'
import { generateUploadURL } from './s3.js'
import cors from 'cors';

const app = express()
app.use(cors());
app.use(express.static('front'))

app.get('/s3Url', async (req, res) => {
  const url = await generateUploadURL()
  res.send({url})
})

app.listen(8088, () => console.log("listening on port 8088"))